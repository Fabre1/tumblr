import java.io.*;

import jxl.*;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;


public class FileRead {

    public void readExcel() throws BiffException, IOException {
        String FilePath = "C:\\Users\\User_2\\Desktop\\acs.xls";
        FileInputStream fs = new FileInputStream(FilePath);
        Workbook wb = Workbook.getWorkbook(fs);

        // TO get the access to the sheet
        Sheet sh = wb.getSheet("accs");

        // To get the number of rows present in sheet
        int totalNoOfRows = sh.getRows();

        // To get the number of columns present in sheet
        int totalNoOfCols = sh.getColumns();

        for (int row = 0; row < totalNoOfRows; row++) {

            for (int col = 0; col < totalNoOfCols; col++) {
                if (col == 0){
                    String tumblrEmail;
                    tumblrEmail = sh.getCell(col, row).getContents();
                    System.out.println("Email: " + tumblrEmail);
                }
                else {
                    String tumblrPass;
                    tumblrPass = sh.getCell(col, row).getContents();
                    System.out.println("Pass: " + tumblrPass);
                }
            }
            System.out.println();
        }
    }

    public static void main(String args[]) throws BiffException, IOException {
        FileRead DT = new FileRead();
        DT.readExcel();
    }
}

